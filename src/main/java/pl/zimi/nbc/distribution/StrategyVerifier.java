package pl.zimi.nbc.distribution;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Responsible for verifing strategy object.
 *
 * @author Szymon Reiter
 */
class StrategyVerifier {

    private static final String SINGULAR_TYPES = "One type can be used at most once.";

    private static final String NOT_POSITIVE_USAGES = "Types: %s have not positive percentages.";

    private static final String SUM_NOT_100 = "Sum of percentages for strategy is equal: %d%%";

    void verify(Strategy strategy) {
        assertSumOfStrategyPercentages(strategy);
        assertOnlyPositivePercentages(strategy);
        assertSingleTypesInStrategy(strategy);
    }

    private void assertSingleTypesInStrategy(Strategy strategy) {
        List<Usage> usages = strategy.getUsages();
        long uniqueTypes = usages.stream()
                .map(Usage::getType)
                .distinct()
                .count();
        if (uniqueTypes != strategy.getUsages().size()) {
            throw new IncorrectStrategyException(SINGULAR_TYPES);
        }
    }

    private void assertOnlyPositivePercentages(Strategy strategy) {
        String notPositiveUsages = strategy.getUsages().stream()
                .filter(it -> it.getPercentage() <= 0)
                .map(it -> it.getType().toString())
                .collect(Collectors.joining(", "));
        if (!notPositiveUsages.isEmpty()) {
            String message = String.format(NOT_POSITIVE_USAGES, notPositiveUsages);
            throw new IncorrectStrategyException(message);
        }
    }

    private void assertSumOfStrategyPercentages(Strategy strategy) {
        IntStream percentagesStream = strategy.getUsages().stream().mapToInt(Usage::getPercentage);
        int sum = percentagesStream.sum();
        if (sum != 100) {
            String message = String.format(SUM_NOT_100, sum);
            throw new IncorrectStrategyException(message);
        }
    }

}
