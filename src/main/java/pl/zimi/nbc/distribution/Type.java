package pl.zimi.nbc.distribution;

/**
 * Enumeration representing different types of funds.
 * In task we have POLISH, FOREIGN, CURRENCY
 *
 * @author Szymon Reiter
 */
public enum Type {
    FOREIGN, CURRENCY, POLISH
}
