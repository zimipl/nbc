package pl.zimi.nbc.distribution;

import org.fest.assertions.api.Assertions;

public class ResultAssertion {

    private Result result;

    public ResultAssertion(Result result) {
        this.result = result;
    }

    public ResultAssertion assertInvestment(Fund fund, int amount) {
        Assertions.assertThat(result.get(fund).getValue()).isEqualTo(amount);
        return this;
    }

    public ResultAssertion assertRest(int amount) {
        Assertions.assertThat(result.getRest().getValue()).isEqualTo(amount);
        return this;
    }
}
