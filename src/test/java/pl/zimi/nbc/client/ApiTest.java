package pl.zimi.nbc.client;

import org.junit.Test;
import pl.zimi.nbc.distribution.*;

import java.util.Arrays;

public class ApiTest {

    private static final Fund PL1 = Fund.builder().id("PL1").name("Fundusz Polski 1").type(Type.POLISH).build();

    private static final Fund PL2 = Fund.builder().id("PL2").name("Fundusz Polski 2").type(Type.POLISH).build();

    private static final Fund PL3 = Fund.builder().id("PL3").name("Fundusz Polski 3").type(Type.POLISH).build();

    private static final Fund ZA1 = Fund.builder().id("ZA1").name("Fundusz Zagraniczny 1").type(Type.FOREIGN).build();

    private static final Fund ZA2 = Fund.builder().id("ZA2").name("Fundusz Zagraniczny 2").type(Type.FOREIGN).build();

    private static final Fund ZA3 = Fund.builder().id("ZA3").name("Fundusz Zagraniczny 3").type(Type.FOREIGN).build();

    private static final Fund PI1 = Fund.builder().id("PI1").name("Fundusz Pieniężny 1").type(Type.CURRENCY).build();

    private static final Strategy SAFE = Strategy.builder().usages(Arrays.asList(
            new Usage(Type.POLISH, 20),
            new Usage(Type.FOREIGN, 75),
            new Usage(Type.CURRENCY, 5)
    )).build();

    private static final String PLN = "PLN";

    @Test
    public void stage1() {
        Request request = Request.builder()
                .amount(new Amount(10000, PLN))
                .strategy(SAFE)
                .funds(Arrays.asList(PL1, PL2, ZA1, ZA2, ZA3, PI1))
                .build();

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertRest(0)
                .assertInvestment(PL1, 1000)
                .assertInvestment(PL2, 1000)
                .assertInvestment(ZA1, 2500)
                .assertInvestment(ZA2, 2500)
                .assertInvestment(ZA3, 2500)
                .assertInvestment(PI1, 500);
    }

    @Test
    public void stage2Example1() {
        Request request = Request.builder()
                .amount(new Amount(10001, PLN))
                .strategy(SAFE)
                .funds(Arrays.asList(PL1, PL2, ZA1, ZA2, ZA3, PI1))
                .build();

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertRest(1)
                .assertInvestment(PL1, 1000)
                .assertInvestment(PL2, 1000)
                .assertInvestment(ZA1, 2500)
                .assertInvestment(ZA2, 2500)
                .assertInvestment(ZA3, 2500)
                .assertInvestment(PI1, 500);
    }

    @Test
    public void stage2Example2() {
        Request request = Request.builder()
                .amount(new Amount(10000, PLN))
                .strategy(SAFE)
                .funds(Arrays.asList(PL1, PL2, PL3, ZA1, ZA2, PI1))
                .build();

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertRest(0)
                .assertInvestment(PL1, 668)
                .assertInvestment(PL2, 666)
                .assertInvestment(PL3, 666)
                .assertInvestment(ZA1, 3750)
                .assertInvestment(ZA2, 3750)
                .assertInvestment(PI1, 500);
    }

}
