package pl.zimi.nbc.distribution;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

@Value
@RequiredArgsConstructor
public class Amount {

    @NonNull
    private int value;

    @NonNull
    private String currency;

    MonetaryAmount toMonetaryAmount() {
        return Money.of(value, currency);
    }

    static Amount from(MonetaryAmount amount) {
        return new Amount(amount.getNumber().intValue(), amount.getCurrency().getCurrencyCode());
    }

}
