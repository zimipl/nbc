package pl.zimi.nbc.distribution;

/**
 * Exception which appears in case strategy is not correct and sum of percentages is different then 100%.
 *
 * @author Szymon Reiter
 */
class IncorrectStrategyException extends RuntimeException {
    IncorrectStrategyException(String message) {
        super(message);
    }
}
