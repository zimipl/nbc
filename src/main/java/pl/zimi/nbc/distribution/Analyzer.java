package pl.zimi.nbc.distribution;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Responsible for providing aggregated facts about request.
 *
 * @author Szymon Reiter
 */

class Analyzer {

    private Request request;

    Analyzer(Request request) {
        this.request = request;
    }

    List<Fund> listFunds(Type type) {
        return request.getFunds().stream().filter(it -> it.getType().equals(type)).collect(Collectors.toList());
    }

    Set<Type> listTypesFromStrategy() {
        return request.getFunds().stream().map(Fund::getType).distinct().collect(Collectors.toSet());
    }

}
