package pl.zimi.nbc.distribution;

import lombok.NonNull;
import lombok.Value;

/**
 * Describes will of distribution of assets for concrete {@link Type} in {@link Strategy}.
 *
 * @author Szymon Reiter
 */
@Value
public class Usage {

    @NonNull
    private Type type;

    private int percentage;

}
