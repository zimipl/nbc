# Aplikacja do wyliczania kwot dla funduszy inwestycyjnych #

Aplikacja spełnia wyszczególnione w specyfikacji założenia.
Przykłady, które zostały przytoczone w specyfikacji zostały spisane
w klasie ApiTest, która przydała się również do wybrania odpowiednich modyfikatorów dostępowych
dla kodu klienckiego.

Porównując plik ApiTest i specyfikacje można zauważyć, że w specyfikacji w **Etap 2 Przykład 2** w tabelce **Wyliczony podział**
wkradł się chochlik. Funduszom 4 i 5 zostało przypisane po 2500 PLN. Podczas gdy
powinny opiewać na 3750 PLN każdy.

Klasa DistributionServiceTest zawiera wszystkie przykłady, które były testowane.

### Użyte biblioteki i narzędzia ###

* Java 8
* JUnit 4
* fest-assert-core - asercje z fluent api
* lombok - projekt generowania kodu na podstawie adnotacji dla prostych klas
* Moneta - kandydat jako Java Money API dla Java 9, oparty na JSR 354
* equalsverifier - biblioteka sprawdzająca poprawność kontraktu equals/hashcode
* Gradle

### Testy ###

Użyto metodologii Test Driven Development. Poszczególne fazy pisania testów,
implementacji i refaktoringów zostały zakończone commitem, aby jawnie ten proces
przedstawić. Intellij wskazał pokrycie na poziomie 100%.

### Inne ###

Starałem się zaimplementować projekt aby był możliwie uniwersalny. Strategie można
tworzyć dowolnie z wykorzystaniem odpowiedniego API.

Nie zostały wykorzystane narzędzia Dependency Injection ze względu na skalę problemu.

Autor świadomie i z premedytacją utworzył wszystkie pliki java w jednym pakiecie jako ćwiczenie
dla siebie do pracy z niedocenianym w środowisku dostępem pakietowym.
Co do założenia w intencji autora miała zostać wykorzystana architektura heksagonalna, niewidoczna
jednak ze względu na skalę problemu. Wydzielanie usług wydawało się niestosowne.

Dostęp publiczny posiadają jedynie klasy, które w założeniu będą wykorzystywane
przez użytkowników biblioteki.

Linki do źródeł:

* https://dzone.com/articles/hexagonal-architecture-is-powerful
* Confitura 2017 - Jakub Nabrdalik - https://youtu.be/EQNZWCkwnAI?t=2m55s
* Confitura 2015 - Bartosz Walacik & Rafał Głowiński - https://www.youtube.com/watch?v=WngirrI5odI