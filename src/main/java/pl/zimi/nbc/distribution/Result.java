package pl.zimi.nbc.distribution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains amounts computed by {@link DistributionService} for  appropriate funds.
 *
 * @author Szymon Reiter
 */
public class Result {

    private Map<Fund, Amount> distribution = new HashMap<>();

    private Amount rest;

    Result(Amount rest) {
        this.rest = rest;
    }

    public Amount get(Fund fund) {
        return distribution.get(fund);
    }

    public Amount getRest() {
        return rest;
    }

    void addInvestments(List<Investment> investments) {
        for (Investment investment : investments) {
            distribution.put(investment.getFund(), investment.getAmount());
        }
    }
}
