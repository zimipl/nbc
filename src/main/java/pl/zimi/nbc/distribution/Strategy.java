package pl.zimi.nbc.distribution;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Representation of distribution strategy for {@link Usage}.
 *
 * @author Szymon Reiter
 */
@Builder
@Value
public class Strategy {

    @NonNull
    private List<Usage> usages;

    int computeDivisor() {
        List<Usage> usages = getUsages();
        List<Integer> percentages = usages.stream().map(Usage::getPercentage).collect(Collectors.toList());
        return 100 / commonGreatesDivisor(percentages);
    }

    private int commonGreatesDivisor(List<Integer> percentages) {
        Integer common = percentages.get(0);
        for (int i = 1; i < percentages.size(); i++) {
            common = commonGreatesDivisor(common, percentages.get(i));
        }
        return common;
    }

    private int commonGreatesDivisor(Integer first, Integer next) {
        // source: https://pl.wikipedia.org/wiki/Algorytm_Euklidesa#Funkcja_wyliczaj.C4.85ca_NWD_w_C.2FC.2B.2B
        int tmp;
        while(next != 0)
        {
            tmp = first % next;
            first = next;
            next = tmp;
        }
        return first;
    }

}
