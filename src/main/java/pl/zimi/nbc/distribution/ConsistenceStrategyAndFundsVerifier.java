package pl.zimi.nbc.distribution;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Verifies if types from strategy and funds declared by user are consistent.
 *
 * @author Szymon Reiter
 */
class ConsistenceStrategyAndFundsVerifier {

    private final static String FUND_NOT_BELONG_STRATEGY = "There is fund: %s which does not belong to strategy.";

    private final static String NO_FUNDS_ASSIGNED = "Type %s does not have any funds.";

    void verify(Analyzer analyzer, Strategy strategy) {
        assertExistFundForType(analyzer, strategy);
        assertFundBelongToStrategy(analyzer, strategy);
    }

    private void assertFundBelongToStrategy(Analyzer analyzer, Strategy strategy) {
        Set<Type> usagesTypes = strategy.getUsages().stream()
                .map(Usage::getType).collect(Collectors.toSet());
        Set<Type> types = new HashSet<>(analyzer.listTypesFromStrategy());
        types.removeAll(usagesTypes);
        if (!types.isEmpty()) {
            Fund fund = analyzer.listFunds(types.iterator().next()).get(0);
            String message = String.format(FUND_NOT_BELONG_STRATEGY, fund.getName());
            throw new InconsistentStrategyAndFundsException(message);
        }
    }

    private void assertExistFundForType(Analyzer analyzer, Strategy strategy) {
        for (Usage usage : strategy.getUsages()) {
            if (analyzer.listFunds(usage.getType()).isEmpty()) {
                String message = String.format(NO_FUNDS_ASSIGNED, usage.getType().toString());
                throw new InconsistentStrategyAndFundsException(message);
            }
        }
    }
}
