package pl.zimi.nbc.distribution;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

/**
 * Representation of fund.
 * It contains basic information about this fund as: id, name and it's type.
 *
 * @author Szymon Reiter
 */
@Builder
@Value
public class Fund {

    @NonNull
    private String id;

    @NonNull
    private String name;

    @NonNull
    private Type type;

}
