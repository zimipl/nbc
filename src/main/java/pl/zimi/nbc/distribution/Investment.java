package pl.zimi.nbc.distribution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
class Investment {

    @NonNull
    private Fund fund;

    @NonNull
    private Amount amount;
}
