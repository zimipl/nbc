package pl.zimi.nbc.distribution;

import javax.money.MonetaryAmount;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Responsible for computing results of distribution.
 *
 * @author Szymon Reiter
 */
class Calculator {

    Result compute(Request request, Analyzer analyzer) {
        MonetaryAmount amount = request.getAmount().toMonetaryAmount();
        MonetaryAmount rest = amount.remainder(request.getStrategy().computeDivisor());
        Result result = new Result(Amount.from(rest));
        MonetaryAmount availableAmount = amount.subtract(rest);
        for (Usage usage : request.getStrategy().getUsages()) {
            MonetaryAmount amountPerType = availableAmount.multiply(usage.getPercentage()).divide(100);
            List<Fund> fundsPerType = analyzer.listFunds(usage.getType());
            List<Investment> investments = prepareInvestments(amountPerType, fundsPerType);
            result.addInvestments(investments);
        }
        return result;
    }

    private List<Investment> prepareInvestments(MonetaryAmount amountPerType, List<Fund> fundsPerType) {
        MonetaryAmount investedAssets = amountPerType.divideToIntegralValue(fundsPerType.size());
        MonetaryAmount first = amountPerType.subtract(investedAssets.multiply(fundsPerType.size())).add(investedAssets);
        List<Investment> investments = fundsPerType.stream()
                .map(fund -> new Investment(fund, Amount.from(investedAssets)))
                .collect(Collectors.toList());
        investments.get(0).setAmount(Amount.from(first));
        return investments;
    }

}
