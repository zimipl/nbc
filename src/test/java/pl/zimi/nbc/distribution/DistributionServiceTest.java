package pl.zimi.nbc.distribution;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import java.util.Arrays;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Test for {@link DistributionService}.
 *
 * @author Szymon Reiter
 */
public class DistributionServiceTest {

    private static final String PLN = "PLN";

    private static final Fund FUND_POLISH_1 = Fund.builder().id("PXP").name("Polish eXtra Profitable").type(Type.POLISH).build();

    private static final Fund FUND_POLISH_2 = Fund.builder().id("PXS").name("Polish eXtra Secure").type(Type.POLISH).build();

    private static final Fund FUND_POLISH_3 = Fund.builder().id("PLV").name("Polish Last Valueable").type(Type.POLISH).build();

    private static final Fund FUND_FOREIGN = Fund.builder().id("FGP").name("Foreign Good Profit").type(Type.FOREIGN).build();

    private static final Fund FUND_CURRENCY = Fund.builder().id("HCF").name("Hard Currency Fund").type(Type.CURRENCY).build();

    private static final Strategy SIMPLE = Strategy.builder().usages(Arrays.asList(new Usage(Type.POLISH, 100))).build();

    private static final Usage POLISH = new Usage(Type.POLISH, 20);

    private static final Usage FOREIGN = new Usage(Type.FOREIGN, 75);

    private static final Usage CURRENCY = new Usage(Type.CURRENCY, 5);

    private static final Usage NEGATIVE = new Usage(Type.POLISH, -10);

    private static final Usage TOO_BIG = new Usage(Type.FOREIGN, 110);

    private static final Usage DOUBLED = new Usage(Type.POLISH, 80);

    private static final Strategy SAFE = Strategy.builder().usages(Arrays.asList(POLISH, FOREIGN, CURRENCY)).build();

    private static final Strategy INCORRECT_NOT_100 = Strategy.builder().usages(Arrays.asList(POLISH, FOREIGN)).build();

    private static final Strategy INCORRECT_NEGATIVE = Strategy.builder().usages(Arrays.asList(NEGATIVE, TOO_BIG)).build();

    private static final Strategy INCORRECT_DOUBLED = Strategy.builder().usages(Arrays.asList(POLISH, DOUBLED)).build();

    private static final Amount BASIC_PLN = new Amount(10000, PLN);

    private static final Amount IRREGURAL_PLN = new Amount(9999, PLN);

    private static final Amount WITH_REST_PLN = new Amount(10001, PLN);

    @Test
    public void shouldPutFullAmountOnOneFund() {
        Request request = request(BASIC_PLN, SIMPLE, FUND_POLISH_1);

        Result result = new DistributionService().scatter(request);

        assertThat(result.get(FUND_POLISH_1)).isEqualTo(BASIC_PLN);
    }

    @Test
    public void shouldPutRespectivelyForEachFundType() {
        Request request = request(BASIC_PLN, SAFE, FUND_POLISH_1, FUND_FOREIGN, FUND_CURRENCY);

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertInvestment(FUND_POLISH_1, 2000)
                .assertInvestment(FUND_FOREIGN, 7500)
                .assertInvestment(FUND_CURRENCY, 500);
    }
    
    @Test
    public void shouldLeaveRest() {
        Request request = request(WITH_REST_PLN, SAFE, FUND_POLISH_1, FUND_FOREIGN, FUND_CURRENCY);

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertRest(1)
                .assertInvestment(FUND_POLISH_1, 2000)
                .assertInvestment(FUND_FOREIGN, 7500)
                .assertInvestment(FUND_CURRENCY, 500);
    }

    @Test
    public void shouldPutEqualPartsOnMoreFunds() {
        Request request = request(BASIC_PLN, SIMPLE, FUND_POLISH_1, FUND_POLISH_2);

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertInvestment(FUND_POLISH_1, 5000)
                .assertInvestment(FUND_POLISH_2, 5000);
    }

    @Test
    public void shouldPutMoreOnFirstFund() {
        Request request = request(BASIC_PLN, SAFE, FUND_POLISH_1, FUND_POLISH_2, FUND_POLISH_3, FUND_FOREIGN, FUND_CURRENCY);

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertInvestment(FUND_POLISH_1, 668)
                .assertInvestment(FUND_POLISH_2, 666)
                .assertInvestment(FUND_POLISH_3, 666)
                .assertInvestment(FUND_FOREIGN, 7500)
                .assertInvestment(FUND_CURRENCY, 500);
    }

    @Test
    public void shouldReturnRestAfterDistributionWithProportionNotPossible() {
        Request request = request(IRREGURAL_PLN, SAFE, FUND_POLISH_1, FUND_FOREIGN, FUND_CURRENCY);

        Result result = new DistributionService().scatter(request);

        new ResultAssertion(result)
                .assertRest(19)
                .assertInvestment(FUND_POLISH_1, 1996)
                .assertInvestment(FUND_FOREIGN, 7485)
                .assertInvestment(FUND_CURRENCY, 499);
    }

    @Test(expected = IncorrectStrategyException.class)
    public void shouldStrategyUsagesSumTo100Percentage() {
        Request request = request(BASIC_PLN, INCORRECT_NOT_100, FUND_POLISH_1, FUND_POLISH_2);

        new DistributionService().scatter(request);
    }

    @Test(expected = IncorrectStrategyException.class)
    public void shouldUsagesBePositive() {
        Request request = request(BASIC_PLN, INCORRECT_NEGATIVE, FUND_POLISH_1, FUND_POLISH_2, FUND_FOREIGN);

        new DistributionService().scatter(request);
    }

    @Test(expected = IncorrectStrategyException.class)
    public void shouldTypesNotBeDoubled() {
        Request request = request(BASIC_PLN, INCORRECT_DOUBLED, FUND_POLISH_1, FUND_POLISH_2);

        new DistributionService().scatter(request);
    }

    @Test(expected = InconsistentStrategyAndFundsException.class)
    public void shouldTypeHaveFund() {
        Request request = request(BASIC_PLN, SAFE, FUND_POLISH_1, FUND_FOREIGN);

        new DistributionService().scatter(request);
    }

    @Test(expected = InconsistentStrategyAndFundsException.class)
    public void shouldFundApplyToStrategy() {
        Request request = request(BASIC_PLN, SIMPLE, FUND_POLISH_1, FUND_FOREIGN);

        new DistributionService().scatter(request);
    }

    @Test
    public void shouldContractBeFulfilled() {
        EqualsVerifier.forClass(Usage.class).verify();
        EqualsVerifier.forClass(Strategy.class).verify();
        EqualsVerifier.forClass(Request.class).verify();
        EqualsVerifier.forClass(Amount.class).verify();
    }

    private Request request(Amount amount, Strategy strategy, Fund... funds) {
        return Request.builder()
                .funds(Arrays.asList(funds))
                .amount(amount)
                .strategy(strategy)
                .build();
    }

}
