package pl.zimi.nbc.distribution;

/**
 * Exception which appears when:
 * - type from strategy has no funds
 * - fund does not belong to any type from strategy
 *
 * @author Szymon Reiter
 */

class InconsistentStrategyAndFundsException extends RuntimeException {
    InconsistentStrategyAndFundsException(String message) {
        super(message);
    }
}
