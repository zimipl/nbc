package pl.zimi.nbc.distribution;

/**
 * Class responsible for handling request of distribution.
 *
 * @author Szymon Reiter
 */
public class DistributionService {

    public Result scatter(Request request) {
        Analyzer analyzer = new Analyzer(request);
        Strategy strategy = request.getStrategy();
        new StrategyVerifier().verify(strategy);
        new ConsistenceStrategyAndFundsVerifier().verify(analyzer, strategy);
        return new Calculator().compute(request, analyzer);
    }

}
