package pl.zimi.nbc.distribution;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

/**
 * Contains all data which are needed in order to peform distribution process.
 *
 * @author Szymon Reiter
 */
@Builder
@Value
public class Request {

    @NonNull
    private List<Fund> funds;

    @NonNull
    private Amount amount;

    @NonNull
    private Strategy strategy;

}
